from datetime import datetime

def queryTemplateEntry(i, entry, id, appname, dynamicValueList):
    yamlEntry = f'''
# {i + 1}
-   pkstatus: true
    dataSource: hasura
    querytemplate: '{entry}'
    pk: {id}
    appid: {appname}
    paramValuesSchema:'''

    j = 0
    while j < len(dynamicValueList):
        dynamicValueString = f'''
        {dynamicValueList[j]}:
            pattern:
            type:'''
        yamlEntry += dynamicValueString
        j += 1

    requiredString = '''
    required:'''
    yamlEntry += requiredString

    k = 0
    while k < len(dynamicValueList):
        requiredValueString = f'''
        - {dynamicValueList[k]}'''
        yamlEntry += requiredValueString
        k += 1
    return yamlEntry

def queryTemplateYamlFile(templateIdArray, templateArray, dynamicValueArrayofArrays, appname):
    timestamp = datetime.now()
    yamlFile = f'''# {timestamp}
---
'''
    i = 0
    while i < len(templateArray):
        yamlEntry = queryTemplateEntry(i, templateArray[i], templateIdArray[i], appname, dynamicValueArrayofArrays[i])
        yamlFile += yamlEntry
        i += 1

    return yamlFile