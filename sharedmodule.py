import os
import re

# Excelsheet
def excelName():
    path = './excels/'
    for file in os.listdir(path):
        if (os.path.isfile(os.path.join(path, file)) and '.xlsx' in file):
            excelName = path + file
    return excelName

def removeNewLine(entry):
    array = entry.split('\n')
    newArray = []
    for lines in array:
        lines = lines.strip()
        newArray.append(lines)
    string = ''
    for items in newArray:
        string += ' ' + items
    newString = string.lstrip()
    return newString

def getArray(df, value, service):
    array = []
    for index, row in df.iterrows():
        i = 0
        while i < len(df.columns):
            if row[i] == value and service == 'MDS':
                if row[i + 3] == '<This will be filled up by AppCS Onboarding Team>' or str(row[i+3]) == 'nan':
                    array.append('<placeholder>')
                else:
                    item = row[i + 3].replace('"${', '${').replace('}"', '}')
                    array.append(removeNewLine(item))
            elif row[i] == value and service == 'MCNS':
                if row[i + 2] == '<This will be filled up by AppCS Onboarding Team>' or str(row[i + 2]) == 'nan':
                    array.append('<placeholder>')
                else:
                    item = row[i + 2].replace('"${', '${').replace('}"', '}')
                    array.append(removeNewLine(item))
            elif row[i] == value and service == 'SFT':
                array.append(row[i])
            i += 1
    return array

def dynamicValueArrayofArrays(array):
    dynamicValueArrayofArrays = []
    for entry in array:
        dynamicValueSet = set()
        entry = entry.split('${')
        for lines in entry:
            lines = lines.split('}')
            for value in lines:
                if len(value.split()) == 1 and re.search("^[A-Za-z0-9_]+$", value):
                    dynamicValueSet.add(value)
        dynamicValueArray = list(dynamicValueSet)
        dynamicValueArrayofArrays.append(dynamicValueArray)
    return dynamicValueArrayofArrays

def fileGeneration(appname, fileContent, extension, documentName):
    documentName = documentName.lower().replace(' ', '')
    if fileContent == 0:
        print(f'{documentName}: Not required')
    else:
        with open(f'./artifacts/{appname}-{documentName}.txt', 'w') as file:
            file.write(fileContent)
        
        txtFile = os.path.join('./artifacts/', f'{appname}-{documentName}.txt')
        newFile = txtFile.replace('.txt', extension)
        os.rename(txtFile, newFile)
        print(f'{documentName}: File Generation Completed')
