from datetime import datetime

def columnArray(df, start):
    startIndex = []
    column = []
    endIndex = []
    for index, row in df.iterrows():
        i = 0
        while i < len(df.columns):
            if row[i] == start:
                startIndex.append(index + 1)
                column.append(i)
            elif row[i] == '<Add more entries here if necessary>':
                endIndex.append(index)
            i += 1
    columnArray = []
    for item in df.iloc[startIndex[0]:endIndex[0], column[0]]:
        columnArray.append(item)
    columnArray = list(map(lambda x: str(x).lower(), columnArray))
    return columnArray

def tableNames(array):
    removeNan = [x for x in array if str(x) != 'nan']
    tableNames = []
    for name in removeNan:
        if name == 'audit_code' or name == 'audit_infotype' or name == 'nric_uuid_main':
            tableNames.append(name)
        elif name.split('_')[0] == 'cd':
            name = name.replace('_', '')
            tableNames.append(name + '_main')
        else:
            tableNames.append(name + '_main')
    return tableNames

def fieldArrayOfArrays(refArray, splitArray):
    fieldArrayOfArrays = []
    array = []
    i = 0
    while i < len(splitArray):
        if i == 0:
            array.append(splitArray[i])
        elif refArray[i] == 'nan' and refArray[i - 1] == 'nan' or refArray[i] == 'nan' and refArray[i - 1] != 'nan':
            array.append(splitArray[i])
        else:
            fieldArrayOfArrays.append(array)
            array = []
            array.append(splitArray[i])
        i += 1
    fieldArrayOfArrays.append(array)
    return fieldArrayOfArrays

def dataContractYamlFile(appname, tableNamesArray, fieldArrayOfArrays):
    timestamp = datetime.now()
    yamlFile = f'''# {timestamp}
---
role: [{appname}]
filter: {{
    it0001_persg: [],
    it0001_werks: []
}}

table:'''

    i = 0
    while i < len(tableNamesArray):
        fieldArrayOfArrays[i] = str(fieldArrayOfArrays[i]).replace("'", '')
        yamlEntry = f'''
-   tablename: {tableNamesArray[i]}
    columns: {fieldArrayOfArrays[i]}
    limit: null
    allow_aggregations: false
'''
        yamlFile += yamlEntry
        i += 1
    
    if yamlFile == f'''# {timestamp}
---
role: [{appname}]
filter: {{
    it0001_persg: [],
    it0001_werks: []
}}

table:''':
        return 0
    else:
        return yamlFile
