# Import Statements
import pandas as pd
from sharedmodule import excelName, removeNewLine, getArray, dynamicValueArrayofArrays, fileGeneration
from mdsqtmodule import queryTemplateYamlFile

# Excelsheet
appname = excelName().split('.xlsx')[0].split('/')[-1]
df = pd.read_excel(excelName(), 'MDS')

# Arrays
templateIdArray = getArray(df, 'Template ID', 'MDS')
templateArray = getArray(df, 'Template', 'MDS')
dynamicValueArrayofArrays = dynamicValueArrayofArrays(templateArray)

# YAML File
queryTemplateYamlFile = queryTemplateYamlFile(templateIdArray, templateArray, dynamicValueArrayofArrays, appname)
queryTemplateFileGeneration = fileGeneration(appname, queryTemplateYamlFile, '.yaml', 'MDS Query Template')