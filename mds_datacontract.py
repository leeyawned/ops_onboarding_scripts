# Import Statements
import pandas as pd
from sharedmodule import excelName, fileGeneration
from mdsdcmodule import columnArray, tableNames, fieldArrayOfArrays, dataContractYamlFile

# Excelsheet
appname = excelName().split('.xlsx')[0].split('/')[-1]
df = pd.read_excel(excelName(), 'MDS')

# Arrays
tableNameColumnArray = columnArray(df, 'Table Name')
tableNamesArray = tableNames(tableNameColumnArray)
fieldColumnArray = columnArray(df, 'Field')
fieldArrayOfArrays = fieldArrayOfArrays(tableNameColumnArray, fieldColumnArray)

# YAML File
dataContractYamlFile = dataContractYamlFile(appname, tableNamesArray, fieldArrayOfArrays)
dataContractFileGeneration = fileGeneration(appname, dataContractYamlFile, '.yaml', 'MDS Data Contract')
